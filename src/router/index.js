import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/productos',
    name: 'Productos',
    component: () => import('../views/Productos.vue')
  },
  {
    path: '/formulario',
    name: 'Formulario',
    component: () => import('../views/Formulario.vue')
  },
  {
    path: '/pagina-error',
    name: 'Error',
    component: () => import('../views/PaginaError.vue')
  },
  {
		path: '*',
		redirect: {
				name: 'Error'
			}
	}
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
